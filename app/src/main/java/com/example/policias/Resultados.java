package com.example.policias;

public class Resultados {
    private String pregunta;
    private double enojo;
    private double disgusto;
    private double miedo;

    public Resultados(String pregunta, double enojo, double disgusto, double miedo) {
        this.pregunta = pregunta;
        this.enojo = enojo;
        this.disgusto = disgusto;
        this.miedo = miedo;
    }

    public String getPregunta() {
        return pregunta;
    }

    public double getEnojo() {
        return enojo;
    }

    public double getDisgusto() {
        return disgusto;
    }

    public double getMiedo() {
        return miedo;
    }
}
